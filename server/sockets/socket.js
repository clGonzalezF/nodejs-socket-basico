const { io } = require('../server');
const { getCurrentTime } = require('../../myTools/currentTime');

io.on('connection', (client) => {
    console.log(`${ getCurrentTime() } || Conectado con Client - - - -`);

    // Enviar al client
    client.emit('sendMessage', {
        user: 'Admin',
        msg: 'Welcome user!'
    });

    client.on('disconnect', () => {
        console.log(`${ getCurrentTime() } (X) Client desconectado - - - -`);
    });

    // Escuchar al client
    client.on('sendMessage', (data, callback) => {
        console.log(`${ getCurrentTime() } (C)`, data);

        //Enviar a todos
        client.broadcast.emit('sendMessage', data);
        /*
        if (msg.user) {
            callback({ resp: 'Success' });
        } else {
            callback({ resp: 'Error in server' });
        }
        */
    });
});