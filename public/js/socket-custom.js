var socket = io();
var historyChat = document.getElementById('historyChat');
var textChat = document.getElementById('textChat');
var userChat = document.getElementById('userChat');

let getCurrentTime = function() {
    let d = new Date();
    return `${d.getHours() }:${ d.getMinutes() }:${ d.getSeconds() }`;
};

// Escucha evento de conexion
socket.on('connect', function() {
    console.log('- - - - Conectado al Server - - - -');
});

// Escucha evento de desconexion
socket.on('disconnect', function() {
    console.log('(X) Conexion perdida con Server - - - -');
});

// Escuchar info
socket.on('sendMessage', function(data) {
    //console.log(data);
    sendMessageChat(data);
    historyChat.firstChild.style.background = "#EEE";
});

// Enviar info
function sendMessageChat(data) {
    let tag = document.createElement("p");
    tag.innerHTML = '<b>' + data.user + '</b> || ' + getCurrentTime() + '<br>' + data.msg;
    if (historyChat.childElementCount == 0) {
        historyChat.appendChild(tag);
    } else {
        historyChat.insertBefore(tag, historyChat.firstChild);
    }
    textChat.value = '';
}

/********** EXTRA **********/
function sendChat(e) {
    socket.emit('sendMessage', {
        user: userChat.value,
        msg: textChat.value
    }, function(resp) {
        console.log('(S)', resp);
    });
    sendMessageChat({ user: userChat.value, msg: textChat.value });
}